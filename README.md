### ytbak

A shell script using `yt-dlp` (https://github.com/yt-dlp/yt-dlp) to perform backups of your personal playlists on YouTube, for example your liked videos.

# Usage

`ytbak.sh youtube_cookies.txt [--download]`,

where youtube_cookies.txt is a Netscape Cookies file containing your login cookies for YouTube. These will be used by `yt-dlp` to access your personal playlists.

A large JSON file with all metadata about your playlist will be created in a folder called `ytbak-<epoch at start>`. If you pass `--download` to the script, then additionally the videos themselves will be downloaded.
