#!/bin/zsh

# Install yt-dlp to use this script and pass it a cookies.txt file for youtube.com
# The cookies.txt file can be obtained for example using this Chrome extension:
# https://chrome.google.com/webstore/detail/get-cookiestxt/bgaddhkoddajcdgocldbbfleckgcbcid

# A large json file containing your liked videos playlist with lots of metadata will be created.
# If you pass --download to the script, the videos themselves will be downloaded.

if [ $# -lt 1 ] || [ $# -gt 2 ]; then
    echo "usage: $0 youtube_cookies.txt [--download]"
    exit 1
fi
COOKIES_FILE=$1
[ "$2" = "--download" ] && DL_VIDEOS=1

set -e

# Select YouTube playlist
# ytfav, ytwatchlater, ytsubs, ythistory, ytrec, ytnotif are also possible (or any public playlist ID)
# The file $COOKIES_FILE at $1 is used to authenticate to access private playlists.
YTBAK_LIST=ytfav
YTBAK_EPOCH=`date +%s`
YTBAK_DIR=ytbak-$YTBAK_EPOCH
echo Starting at $YTBAK_EPOCH \(`date`\)
mkdir -p $YTBAK_DIR

trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT
(while true; do
    sleep 2
    echo -ne "\rProcessed: `wc -l $YTBAK_DIR/$YTBAK_LIST-$YTBAK_EPOCH.json`" \
             "   errors: `wc -l $YTBAK_DIR/errs-$YTBAK_EPOCH.txt`"
done) &
if [ "$DL_VIDEOS" -eq 1 ]; then
    yt-dlp --cookies $COOKIES_FILE :$YTBAK_LIST --dump-json --no-simulate \
        -o "videos/%(title)s [%(id)s].%(ext)s" \
        > $YTBAK_DIR/$YTBAK_LIST-$YTBAK_EPOCH.json \
        2> $YTBAK_DIR/errs-$YTBAK_EPOCH.txt
else
    yt-dlp --cookies $COOKIES_FILE :$YTBAK_LIST --dump-json \
        > $YTBAK_DIR/$YTBAK_LIST-$YTBAK_EPOCH.json \
        2> $YTBAK_DIR/errs-$YTBAK_EPOCH.txt
fi
